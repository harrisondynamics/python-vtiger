import json
import mimetypes
import os


class Ticket:
    def __init__(self, client):
        self.client = client

    def list_tickets(self, **conditions):
        conditions_string = ''
        for condition in conditions:
            conditions_string += f"{condition} IN ({conditions[condition]}) AND "

        conditions_string = conditions_string.removesuffix(' AND ')

        query = f"SELECT * FROM HelpDesk WHERE {conditions_string} ORDER BY modifiedtime DESC;"

        tickets = self.client.request('query', {
            'query': query,
        })

        try:
            tickets = tickets['result']
        except (KeyError, ValueError, TypeError):
            tickets = []

        return tickets

    def get_ticket(self, ticket_id):
        ws_ticket_id = self.client.format_webservice_id(ticket_id, 'HelpDesk')
        ticket = self.client.request('retrieve', {
            'id': ws_ticket_id,
        })

        try:
            ticket = ticket['result']
            # ticket = self.client.convert_to_date(ticket, 'modifiedtime')
            # ticket = self.client.convert_to_date(ticket, 'createdtime')
        except (KeyError, ValueError, TypeError):
            ticket = None

        query = "SELECT * FROM ModComments WHERE related_to='{0}' ORDER BY modifiedtime DESC;".format(ws_ticket_id)
        comments = self.client.request('query', {
            'query': query,
        })

        try:
            comments = comments['result']
        except (KeyError, ValueError, TypeError):
            comments = []

        return ticket, comments

    def create_ticket(self, data):
        request = {
            'elementType': 'HelpDesk',
            'element': json.dumps(data),
        }

        created_ticket = self.client.request('create', request, 'POST')

        if created_ticket and created_ticket.get('success', False) and created_ticket.get('result'):
            return created_ticket['result']

        return None

    def update_ticket(self, data):
        request = {
            'elementType': 'HelpDesk',
            'element': json.dumps(data),
        }

        created_ticket = self.client.request('revise', request, 'POST')

        if created_ticket and created_ticket.get('success', False) and created_ticket.get('result'):
            return created_ticket['result']

        return None

    def create_comment(self, data):
        request = {
            'elementType': 'ModComments',
            'element': json.dumps(data),
        }

        created_comment = self.client.request('create', request, 'POST')

        if created_comment and created_comment.get('success', False) and created_comment.get('result'):
            return created_comment['result']

        return None

    def create_document(self, data):
        filename = os.path.basename(data['file'].name)
        mimetype = mimetypes.guess_type(filename)

        try:
            mimetype = mimetype[0]
        except IndexError:
            mimetype = 'unknown'

        request = {
            'elementType': 'Documents',
            'element': json.dumps({
                'notes_title': filename,
                'filename': filename,
                'filetype': mimetype,
                'filesize': os.fstat(data['file'].fileno()).st_size,
                'filelocationtype': 'I',
                'filedownloadcount': 0,
                'filestatus': 1,
                'folderid': self.client.format_webservice_id(1, 'Folder'),
                'assigned_user_id': self.client.format_webservice_id(1, 'Users'),
                'documenttype': data['documenttype'],
            }),
        }

        created_document = self.client.request('create', request, 'POST', files={
            'file': (filename, data['file'], mimetype, {'Expires': '0'})
        })

        if created_document and created_document.get('success', False) and created_document.get('result'):
            created_document = created_document['result']
        else:
            return None

        self.client.request('upload_helpdesk_document', {
            'element': json.dumps({
                'ticket_id': data['ticket_id'],
                'notes_id': self.client.extract_webservice_id(created_document['id']),
            })
        }, 'POST')

        return created_document
