import hashlib
import json
import time
from datetime import datetime

import requests
from requests.adapters import HTTPAdapter
from urllib3 import Retry
from requests import HTTPError, ConnectionError


class Vtiger:
    url_pattern = '%s/webservice.php'

    # sessions are valid for 1 day in vtiger, but they also expire if no request has been made for 30 mins,
    # so we're keeping both these values and if one of them expires, we need to refresh the session
    session_short_lifetime = 1800
    session_long_lifetime = 86400

    def __init__(self, username, access_key, base_url, storage, module_ids, **kwargs):
        """
        :param username: Vtiger username
        :param access_key: Vtiger access key
        :param base_url: Base URL of Vtiger instance (must not include '/webservices.php')
        :param storage: A cache storage where session tokens will be saved.
        :param module_ids: A dictionary that contains the webservice module IDs (eg: {'Users': 19, 'HelpDesk': 17})
        :param kwargs: Any other params
        """
        self.username = username
        self.access_key = access_key
        self.url = self.url_pattern % base_url
        self.storage = storage
        self.module_ids = module_ids
        self.timeout = kwargs.get('timeout', 10)
        self.logger = kwargs.get('logger')

    def _log(self, *args, **kwargs):
        if not self.logger:
            return

        self.logger.log(*args, **kwargs)

    def _save_cache(self, name, value):
        json_value = json.dumps(value)

        self.storage.set(name, json_value)

    def _get_cache(self, name, default=None):
        value = self.storage.get(name)

        if value is None:
            return default

        return json.loads(value)

    def _request(self, data, method='GET', headers=None, files=None):
        verb_params = {
            'params': data,
        }

        if method in ['POST', 'PUT']:
            verb_params = {
                'data': data,
            }

        try:
            s = requests.Session()
            retries = Retry(total=5, connect=2, read=2, redirect=5)
            s.mount(self.url, HTTPAdapter(max_retries=retries))
            response = s.request(method, '%s' % self.url, timeout=self.timeout, headers=headers, files=files, **verb_params)
        except HTTPError as e:
            try:
                return None
            except AttributeError:
                return None
        except ConnectionError:
            return None

        try:
            json_response = response.json()
        except (ValueError, TypeError) as e:
            self._log('error', f'Invalid JSON from Vtiger on {self.url}', exc_info=e)
            return None

        return json_response

    def login(self):
        # check if there is a previous session and if it's still valid
        vtiger_data = self._get_cache('vtiger')

        if vtiger_data:
            now = time.time()
            session_data = vtiger_data.get('session')

            if session_data and session_data.get('session_name'):
                session_created = session_data.get('created', 0)
                session_expires = session_data.get('expires', 0)

                if session_created + self.session_short_lifetime > now and session_expires > now:
                    # we have a valid session (for now)

                    # since dictionaries are reference-type, changing this, will also change vtiger_data
                    session_data['created'] = now
                    self._save_cache('vtiger', vtiger_data)

                    return session_data['session_name']

        values = {'operation': 'getchallenge', 'username': self.username}
        response = self._request(values)

        try:
            token = response['result']['token']
        except (KeyError, TypeError):
            raise LookupError("Unable to login. Can't get challenge token.")

        # use the token + accesskey to create the tokenized accessKey
        key = hashlib.md5(str(token).encode('utf-8') + str(self.access_key).encode('utf-8'))
        tokenized_access_key = key.hexdigest()
        values['accessKey'] = tokenized_access_key

        # now that we have an accessKey tokenized, let's perform a login operation
        values['operation'] = 'login'
        response = self._request(values, 'POST')

        now = time.time()
        # set the sessionName
        try:
            session_name = response['result']['sessionName']
        except (KeyError, TypeError):
            raise LookupError("Unable to login. Token is invalid.")

        vtiger_data = self._get_cache('vtiger', {})

        session_data = {
            'session_name': session_name,
            'created': now,
            'expires': now + self.session_long_lifetime,
        }
        vtiger_data['session'] = session_data
        self._save_cache('vtiger', vtiger_data)

        return session_name

    def request(self, operation, data=None, method='GET', headers=None, files=None):
        if data is None:
            data = {}

        try:
            session_name = self.login()
        except LookupError:
            return None

        data['operation'] = operation
        data['sessionName'] = session_name

        result = self._request(data, method, headers, files)

        return result

    def format_webservice_id(self, entity_id, entity_type):
        return f'{self.module_ids[entity_type]}x{entity_id}'

    def extract_webservice_id(self, webservice_id):
        try:
            return webservice_id.split('x')[1]
        except IndexError:
            return None

    # @staticmethod
    # def convert_to_date(results, key):
    #     if isinstance(results, list):
    #         for result in results:
    #             try:
    #                 result[key] = datetime.strptime(result[key], '%Y-%m-%d %H:%M:%S')
    #             except (ValueError, KeyError):
    #                 pass
    #     else:
    #         try:
    #             results[key] = datetime.strptime(results[key], '%Y-%m-%d %H:%M:%S')
    #         except (ValueError, KeyError):
    #             pass
    #
    #     return results
