import pathlib

from vtiger.client import Vtiger
import redis

from vtiger.entities import Ticket

HERE = pathlib.Path(__file__).parent.resolve()
ENV = 'local'
CRM_IDS = {
    'Accounts': 11,
    'HelpDesk': 17,
    'Users': 19,
    'ProcurementContracts': 58,
    'Folder': 22,
}

if ENV == 'prod':
    username = 'portal'
    password = 'rivUkr0lhFpvNO8q'
    url = 'https://crm.academiadeadministratie.ro'
elif ENV == 'stg':
    username = 'api'
    password = 'BvLDzIptrUCYApmH'
    url = 'http://192.168.10.128'
else:
    username = 'portal'
    password = 'rivUkr0lhFpvNO8q'
    url = 'http://crm.harrison.local'

cache = redis.Redis(host='localhost', port=6379, db=0)

client = Vtiger(username, password, url, cache, CRM_IDS)

tickets = Ticket(client)


def get_ticket(tickets, ticket_id):
    return tickets.get_ticket(ticket_id)


def upload_document(tickets, ticket_id):
    f = open(HERE / 'tmp' / 'test.txt', 'r')

    document_data = {
        'documenttype': 'Altele',
        'ticket_id': ticket_id,
        'file': f,
    }

    document = tickets.create_document(document_data)
    f.close()

    return document


def create_ticket():
    ticket = tickets.create_ticket({
        'ticket_title': 'test from client',
        'portal_id': 12,
        'ticketpriorities': 'High',
        'ticketcategories': 'Other Problem',
        'description': 'some ticket from client',
        'parent_id': client.format_webservice_id(4451882, 'Accounts'),
        'assigned_user_id': client.format_webservice_id(1, 'Users'),
        'ticketstatus': 'Open',
        'servicetype': 'SCIM',
        'contact_id': 0,
        'from_portal': 1,
        # 'contractid': client.format_webservice_id(4758984, 'ProcurementContracts')
    })

    return ticket


def create_comment():
    comment = tickets.create_comment({
        'related_to': 4760660,
        'portal_id': 1,
        'commentcontent': 'comment from client',
        'assigned_user_id': client.format_webservice_id(1, 'Users'),
    })

    return comment


def update_ticket():
    ticket = tickets.update_ticket({
        'ticket_title': 'test from client edited',
        'id': client.format_webservice_id(4760660, 'HelpDesk')
    })

    return ticket


update_ticket()
