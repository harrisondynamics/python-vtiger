import pathlib
from setuptools import setup

# The directory containing this file
HERE = pathlib.Path(__file__).parent

# The text of the README file
README = (HERE / "README.md").read_text()

# This call to setup() does all the work
setup(
    name="python-vtiger",
    version="1.0.0",
    description="Library for interfacing with Vtiger Webservices",
    long_description=README,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/harrisondynamics/python-vtiger/",
    author="Eduard Luca",
    author_email="eduard.luca@edus.ro",
    license="MIT",
    classifiers=[
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.8",
    ],
    packages=["vtiger"],
    include_package_data=True,
    install_requires=["requests", "urllib3"],
    entry_points={},
)
